import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const dialog = ref(false);
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res);
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveProducts() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = true;
      clearProduct();
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  function clearProduct() {
    editedProduct.value = { name: "", price: 0 };
  }
  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProducts,
    editProduct,
    deleteProduct,
  };
});
