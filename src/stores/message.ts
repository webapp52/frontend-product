import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const showMessage = ref(false);
  const message = ref("");

  function showError(test: string) {
    message.value = test;
    showMessage.value = true;
  }

  return { showError, message, showMessage };
});
